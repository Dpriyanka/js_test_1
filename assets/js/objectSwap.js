var emp = {
	id: 101,
	fname: 'priya',
	lname: 'devlekar',
	department: 'IT'
};

console.log("original emp object");
console.log(emp);
function swap(obj) {
   var result = {};
   var keys = keys_data(obj);
   var i=0, length = keys.length
   for (i; i < length; i++) {
     result[obj[keys[i]]] = keys[i];
   }
   return result;
 }

function keys_data(obj)
 {
   if (!isObject(obj)) return [];
   if (Object.keys) return Object.keys(obj);
   var keys = [];
   for (var key in obj){
     if (_.has(obj, key)) keys.push(key);
   }
   return keys;
 }
function isObject(obj)
{
   var type = typeof obj;
   return type === 'function' || type === 'object' && !!obj;
 }
console.log("copy of Emp object")  
console.log(swap(emp));
